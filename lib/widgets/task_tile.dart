import 'package:flutter/material.dart';

class TaskTile extends StatelessWidget {
  final bool isChecked;
  final String taskName;
  final toggleCheckboxState;
  final deleteTile;

  const TaskTile(
      {super.key,
      required this.taskName,
      required this.isChecked,
      this.toggleCheckboxState,
      this.deleteTile});

  @override
  Widget build(BuildContext context) {
    return ListTile(
        title: Text(
          taskName,
          style: TextStyle(
              fontSize: 20,
              decoration: isChecked ? TextDecoration.lineThrough : null),
        ),
        onLongPress: deleteTile,
        trailing: Checkbox(
          activeColor: Colors.lightBlueAccent,
          value: isChecked,
          onChanged: toggleCheckboxState,
        ));
  }
}

// class TaskCheckbox extends StatelessWidget {
//   const TaskCheckbox(
//       {super.key, this.toggleCheckboxState, required this.checkboxState});

//   final bool checkboxState;
//   final toggleCheckboxState;
//   @override
//   Widget build(BuildContext context) {
//     return Checkbox(
//       activeColor: Colors.lightBlueAccent,
//       value: checkboxState,
//       onChanged: toggleCheckboxState,
//     );
//   }
// }