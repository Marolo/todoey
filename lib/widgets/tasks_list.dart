import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoey/models/task_data.dart';
import 'package:todoey/widgets/task_tile.dart';

class TasksList extends StatelessWidget {
//   const TasksList({super.key, required this.tasks});

//   final List<Task> tasks;

//   @override
//   State<TasksList> createState() => _TasksListState();
// }

// class _TasksListState extends State<TasksList> {
  @override
  Widget build(BuildContext context) {
    return Consumer<TaskData>(builder: (context, taskData, child) {
      return ListView.builder(
        itemBuilder: (context, index) {
          final task = taskData.tasks[index];
          // print(context);
          return TaskTile(
            taskName: task.name,
            isChecked: task.isDone,
            toggleCheckboxState: (_) => taskData.updateTask(task),
            deleteTile: () => taskData.deleteTask(index),
          );
        },
        itemCount: taskData.taskCount,
      );
    });
  }
}
